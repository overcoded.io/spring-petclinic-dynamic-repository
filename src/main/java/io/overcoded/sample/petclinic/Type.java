package io.overcoded.sample.petclinic;


import io.overcoded.repository.annotation.DynamicRepository;
import io.overcoded.repository.annotation.FindAllBy;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@DynamicRepository
@EqualsAndHashCode(of = "id")
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Length(max = 80)
    @FindAllBy(modifier = "ContainingIgnoreCase")
    private String name;
}
