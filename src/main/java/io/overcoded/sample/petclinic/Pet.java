package io.overcoded.sample.petclinic;

import io.overcoded.repository.annotation.DynamicRepository;
import io.overcoded.repository.annotation.FindAllBy;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
@Entity
@DynamicRepository
@EqualsAndHashCode(of = "id")
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Length(max = 30)
    @FindAllBy(modifier = "ContainingIgnoreCase")
    private String name;

    private LocalDate birthDate;

    @ManyToOne
    private Type type;

    @ManyToOne
    private Owner owner;
}
