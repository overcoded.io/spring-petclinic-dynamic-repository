package io.overcoded.sample.petclinic;

import io.overcoded.repository.annotation.DynamicRepository;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@DynamicRepository
@EqualsAndHashCode(of = "id")
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Pet pet;

    @NotNull
    private LocalDate visitDate;

    @NotBlank
    @Length(max = 255)
    private String description;
}
