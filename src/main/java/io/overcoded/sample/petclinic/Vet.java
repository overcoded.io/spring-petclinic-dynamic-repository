package io.overcoded.sample.petclinic;

import io.overcoded.repository.annotation.DynamicRepository;
import io.overcoded.repository.annotation.FindAllBy;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Entity
@DynamicRepository
@EqualsAndHashCode(of = "id")
public class Vet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Length(max = 30)
    private String firstName;

    @NotBlank
    @Length(max = 30)
    @FindAllBy(modifier = "ContainingIgnoreCase")
    private String lastName;

    @ManyToMany
    @JoinTable(
            name = "vet_specialty",
            joinColumns = @JoinColumn(name = "vet_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "specialty_id", referencedColumnName = "id")
    )
    private List<Specialty> specialties;
}
